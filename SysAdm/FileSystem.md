 Домашнее задание к занятию "3.5. Файловые системы"
1. файлы, являющиеся жесткой ссылкой на один объект не могут иметь разные права доступа и владельца т.к. inode у них общий.
   изменяя права доступа или владельца информация записывается в метаданные туда же где и inode 
1. Это такие файлы, которые занимают меньше дискового пространства, чем их собственный размер.
   
    Следующие ФС поддерживают разрежённые файлы: *BTRFS, NILFS, ZFS, NTFS[2], ext2, ext3, ext4, XFS, JFS, ReiserFS, Reiser4, UFS, Rock Ridge, UDF, ReFS, APFS*.
1.
1. Для проверки количества устройств подключённых к SATA-контроллеру можно применить команду
  
      `ls /dev | grep sd`
    
      `fdisk /dev/sdb` создаём разделы на  /dev/sdb 
     
1. переносим данную таблицу разделов на второй диск 
       
       `sfdisk --dump /dev/sdb > sdb.dump` 
       
       `sfdisk /dev/sdc < sdb.dump`
1. Сначала необходимо занулить суперблоки на дисках, 
   которые мы будем использовать для построения RAID 
   (если диски ранее использовались, их суперблоки могут 
   содержать служебную информацию о других RAID):
   
      `mdadm --zero-superblock --force /dev/sd{b,c}`

   создаём массив RAID 1 из двух разделов: один из sdb5 и второй из sdc5

      `mdadm --create /dev/md1 --level=1 --raid-devices=2 /dev/sdb5 /dev/sdc5`

        `--create (или -С): команда создания`

        `/dev/md1: имя устройства создаваемого виртуального раздела`

        `    --level=1 (или сокращённо -l 1): уровень RAID. `

        `   --raid-devices=2 (или сокращённо -n 2): количество устройств`

        `  /dev/sdb5: первый диск в массиве`

        ` /dev/sdc5: второй диск`

   Подробную информацию о конкретном массиве можно посмотреть командой:

      `mdadm -D /dev/md0` или 
      `cat /proc/mdstat`
   
1.  создаём массив RAID 0
  
    `sudo mdadm --create /dev/md0 --level=0 --raid-devices=2 /dev/sdb1 /dev/sdc1`

1.    два независимых PV на получившихся md-устройствах 
     
       `pvcreate /dev/md1 /dev/md0`
      
      смотрим что получилось
      
        `pvdisplay` 

1.    Группа томов - это не что иное, как пул памяти, который будет распределен 
      между логическими томами и может состоять из нескольких 
      физических разделов
     
      `vgcreate volgrpmd /dev/md1 /dev/md0`

       смотрим что получилось
      
       `vgdisplay`

1.    LV размером 100 Мб
      
      `lvcreate -l 25 -n logvol volgrpmd`
      
      `lvdisplay`
    
1.     Создаём ФС на получившемся LV
   
      ` mkfs.ext4 /dev/volgrpmd/logvol`

1.     Монтируем
   
      `mount /dev/volgrpmd/logvol /mnt`

1.     wget https://mirror.yandex.ru/ubuntu/ls-lR.gz -O /tmp/new/test.gz
1.     lsblk
               `NAME                  MAJ:MIN RM  SIZE RO TYPE  MOUNTPOINT
            sda                     8:0    0   64G  0 disk
            ├─sda1                  8:1    0  512M  0 part  /boot/efi
            ├─sda2                  8:2    0    1K  0 part
            └─sda5                  8:5    0 63.5G  0 part
              ├─vgvagrant-root    253:0    0 62.6G  0 lvm   /
              └─vgvagrant-swap_1  253:1    0  980M  0 lvm   [SWAP]
            sdb                     8:16   0  2.5G  0 disk
            ├─sdb1                  8:17   0  512M  0 part
            │ └─md0                 9:0    0 1020M  0 raid0
            │   └─volgrpmd-logvol 253:2    0  100M  0 lvm   /mnt
            ├─sdb2                  8:18   0    1K  0 part
            └─sdb5                  8:21   0    2G  0 part
              └─md1                 9:1    0    2G  0 raid1
            sdc                     8:32   0  2.5G  0 disk
            ├─sdc1                  8:33   0  512M  0 part
            │ └─md0                 9:0    0 1020M  0 raid0
            │   └─volgrpmd-logvol 253:2    0  100M  0 lvm   /mnt
            ├─sdc2                  8:34   0    1K  0 part
            └─sdc5                  8:37   0    2G  0 part
              └─md1                 9:1    0    2G  0 raid1`
1.  ```bash
    root@vagrant:~# gzip -t /tmp/new/test.gz
    root@vagrant:~# echo $?
    0
    ```
1.    переместим содержимое PV с RAID0 на RAID1
      
      `pvmove /dev/md0 /dev/md1`
1.    `mdadm /dev/md1 --fail /dev/sdc5`
1.  `dmesg` 
    
     md/raid1:md1: Disk failure on sdc5, disabling device.
       
     md/raid1:md1: Operation continuing on 1 devices.
      

      
   
   
   
   
   
   
   
   